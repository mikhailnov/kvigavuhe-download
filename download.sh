#!/usr/bin/env bash
# Quick, dirty ang ugly script to download from knigavuhe.com

set -e

input="$1"
input="${input:-"$(pwd)/input.txt"}"
download_dir="${download_dir:-"$(pwd)/DOWNLOADED"}"

cat_list(){
	cat "$input" | sed -e 's,\",\n,g' | grep '\.mp3$' | sed -e 's,\\,,g'
}
 
mk_unique_list(){
	cat_list | awk -F '/' '{print $(NF-1)}' | sort -u
}

mk_dirs(){
	while read -r line
	do
		if [ ! -d "${download_dir}/${line}" ]; then
			mkdir -p "${download_dir}/${line}"
		fi
	done < <(mk_unique_list)
}

download(){
	pushd "$download_dir"
	while read -r line
	do
		if [ ! -f "$line" ]; then
			axel -n 5 "$line"
		fi
	done < <(cat_list)
	popd
}

parallel_download(){
	pushd "$download_dir"
	while read -r unique_dir
	do
		pushd "$unique_dir"
		parallel -a <(cat_list | grep "/${unique_dir}/") -P 16 wget "{1}"
		popd
	done < <(mk_unique_list)
	popd
}

mv_to_dirs(){
	pushd "$download_dir"
	while read -r unique
	do
		while read -r line1
		do
			mv -v "$line1" "${download_dir}/${unique}"
		done < <(find . -maxdepth 1 -type f -iname "*${unique}*")
	done < <(mk_unique_list)
	popd
}

mk_dirs
#download
parallel_download
